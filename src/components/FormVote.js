import { useEffect, useState } from "react";
import { Container, Row, Col, Form, Card, FormCheck, Button } from 'react-bootstrap';
import {
    susbcribeToEvent,
    suscribeToAllEventQuestions,
    createConnectedUser,
} from "../realtimeDatabase";
import { useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import logo from '../assets/img/logo_webAsamblea.png';


const FormVote = () => {
    const [, setEvent] = useState();
    const [questions, setQuestions] = useState();
    const [selectedValue, setSelectValue] = useState();
    const [posicion, setPosicion] = useState(0);
    const [ errorform, setErrorForm ] = useState(false);

    
let { state } = useLocation();
    console.log("sessionStorage", JSON.parse(localStorage.getItem('userSession')));
    // console.log("form", state.dataForm, state.dataUser);

    /* TODO:
        1- Establecer sesion cuando vote 
        localStorage.setItem('userSession', JSON.stringify({userData: data.payload, voted: true }));

        2- Recuperar sesion para verificar si ya voto
          Obtener la sesion: JSON.parse(localStorage.getItem('userSession'))
          Validar si voto: nombreVariable.vote <---

          Lo anterior se hace unicamente en el primer render del componente, pista useEffect y listado de estados escuchados


    */

    useEffect(() => {
    susbcribeToEvent("QxO0wvQol37isDH7nZST", (doc) => {
    setEvent(doc);
    });


    suscribeToAllEventQuestions("QxO0wvQol37isDH7nZST", (questions) => {
    setQuestions(questions);

    });

    createConnectedUser("QxO0wvQol37isDH7nZST", "1", true);

    return () => {
    createConnectedUser("QxO0wvQol37isDH7nZST", "1", false);
    };
    }, []);

    console.log(questions)
    return (
    <div className="App container-fluid">

    <section className="container logo">
            <img src={logo}></img>
    </section>


    {questions && posicion < questions.length ? 
        ( questions[posicion].isOpen ? ( 
        <>
        {questions[posicion].question }
        <Formik
            initialValues={{
                picked: '',
            }}

            validate={(values) => {
                let errores = {};

                if (!values.picked) {
                    errores.picked= 'Seleccione una opción'
                }
                return errores;
            }}

            onSubmit={(valores) =>{
                console.log("valors", valores);
                console.log('votar');
            }}

        >
            {( { values, errors, touched, handleSubmit}) => (
                <Container>
                <Row>
                <Col lg={3} />
                <Col>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-6" controlId="formBasicCheckbox" onSubmit={() => { console.log('He votado, aqui es donde establezco la sesion')}}>
                        <Card className={selectedValue === "si" ? "card-style-active" : "card-style" } onClick={() => setSelectValue("si")}>
                            <Card.Body className="container "> 
                            <FormCheck 
                                name= "picked"
                                type="radio" 
                                label="Si" 
                                value= {values.picked}
                                className="active" 
                                onClick={() => setSelectValue("si") }  
                                checked={selectedValue === "si"} />
                            </Card.Body>
                        </Card>
                        <Card className={selectedValue === "no" ? "card-style-active" : "card-style" } onClick={() => setSelectValue("no")}>
                            <Card.Body className="container">
                            <FormCheck 
                            name= "picked" 
                            type="radio" 
                            label="No" 
                            value={values.picked} 
                            onClick={() => setSelectValue("no")} 
                            checked={selectedValue === "no"} />
                            </Card.Body>
                        </Card>
                        </Form.Group>
                        {errorform && <div className='error '>Seleccione una opción</div>}
                    </Form>
                </Col>
                <Col lg={3} />
                </Row>
            </Container>
            )}
        </Formik>
        <br />
        {(posicion ) <= questions.length && questions[posicion].isOpen &&
        
        <Formik>
            <Container>
                <Row>
                    <Col lg={3} />
                    <Col lg={6}>
                    <Button variant="primary" className="button" onClick={() =>{
                        if (selectedValue === "si" || selectedValue === "no") {
                            setPosicion(posicion + 1)
                            setSelectValue("");
                            setErrorForm(false);
                        }else{ 
                            setErrorForm(true);
                        }
                    
                    }}>Votar  
                    </Button>
                </Col>
                <Col lg={3} />
                </Row>
            </Container>
        </Formik>

        }
        </>
        )
        :
    <p>Gracias por utilizar el servicio </p>
    ): <p>Gracias Por participar</p>}   

    </div>
    );

}

export default FormVote;