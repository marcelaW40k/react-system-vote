import React, {useState} from 'react';
import '../App.css';
import { Form, Button, Row, Col, Container, Alert } from 'react-bootstrap';
import { Formik } from 'formik';
import { useNavigate } from "react-router-dom";
import { getUserDocIdAsync } from '../services/services';


const FormRegister = () => {
    const [ formSend, setFormSend] = useState(false);
    const [ dataUser, setDataUser] = useState(null);
    const [ error, setError] = useState(false);
    const [ errorMessage, setErrorMessage ] = useState('');
    const [ dataForm, setDataForm ] = useState('');

    
    const navigate = useNavigate();
    
    

    return (
        <>
        <section>
                    <h2>Formulario Registro</h2>
                </section>
            <Formik
                initialValues={{
                    cedula: ''
                }}
                validate={(values) => {
                    let errores = {};

                    if (!values.cedula) {
                        errores.cedula= 'Por favor ingresa un número de cédula'
                    }
                    return errores;
                }}
                onSubmit={(values, {resetForm}) => {
                    resetForm();
                    const body = {eventId: parseInt("4", 10), docId: values.cedula, email: values.email, phoneNumber: values.phoneNumber}
                    getUserDocIdAsync(body).then(data => {
                        if(data.status === 'error'){
                            console.log(data);
                            setErrorMessage(data.errorMessage.message);
                            setError(true);
                        }
                        if(data.status === 'success') 
                            {localStorage.setItem('userSession', JSON.stringify({userData: data.payload, voted: true }));
                            setDataUser(data);
                            navigate('/formvote', { state: {
                                dataForm: body,
                                dataUser: data.payload
                            }
                            })
                        }
                    });


                    console.log('Formulario enviado', values);
                    setFormSend(true);
                    setTimeout(() => setFormSend(false), 5000);
                }}
            >
                { ({values, errors, touched, handleSubmit, handleChange, handleBlur}) => (
                    <Container>
                    <Row>
                    <Col lg={4}></Col>
                        <Col lg={4}>
                        <Form className='Forms' onSubmit={handleSubmit}>
                            {error && 
                            <div>
                                <Alert className="alertError"  variant="danger">
                                    <p>
                                    {errorMessage}
                                    </p>
                                </Alert>
                                </div>}
                            <Form.Group className="mb-3" controlId="formBasicNumber">
                                <Form.Control 
                                type="text" 
                                name= "cedula" 
                                placeholder="Cedula" 
                                value={values.cedula}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                />
                                {touched.cedula && errors.cedula && <div className='error '>{errors.cedula}</div>}
                            </Form.Group>
                        <Button variant="primary" type="submit">
                            Enviar
                        </Button>
                        {formSend && <p className='exito'>Formulario enviado con exito</p>}
                        </Form>
                        </Col>
                        <Col lg={4}></Col>
                    </Row>
                </Container>
                )}
                
                
            </Formik>
        </>
    );

}

export default FormRegister;