import {
  collection,
  addDoc,
  getFirestore,
  onSnapshot,
  doc,
  query,
} from "firebase/firestore";
import "./firebase";

const db = getFirestore();

export function createEvent(eventId, name) {
  addDoc(collection(db, "events"), {
    name,
  });
}

export function createConnectedUser(eventId, userId, isConnected) {
  addDoc(collection(db, "events", eventId, "connected_users"), {
    isConnected,
    userId,
  });
}

export function susbcribeToEvent(eventId, callback) {
  return onSnapshot(doc(db, "events", eventId), (doc) => {
    console.log("Current data: ", doc.data());
    callback(doc.data());
  });
}

export function susbcribeToEventQuestion(eventId, questionId, callback) {
  return onSnapshot(doc(db, "events", eventId, "questions", "*"), (doc) => {
    console.log("Current data: ", doc.data());
    callback(doc.data());
  });
}

export function suscribeToAllEventQuestions(questionId, callback) {
  const q = query(collection(db, "events", questionId, "questions"));

  return onSnapshot(q, (querySnapshot) => {
    const questions = [];
    querySnapshot.forEach((doc) => {
      questions.push(doc.data());
    });

    callback(questions);
  });
}
