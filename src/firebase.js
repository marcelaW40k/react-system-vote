import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAhcPC79i9kQ4Vjl83k3zV4VavhBzxb8lo",
  authDomain: "app-webasamblea.firebaseapp.com",
  projectId: "app-webasamblea",
  storageBucket: "app-webasamblea.appspot.com",
  messagingSenderId: "184860247546",
  appId: "1:184860247546:web:293488795c33debfac9043",
  measurementId: "G-8NN5YMJQF0",
};

export const firebaseApp = initializeApp(firebaseConfig);
