import React from "react";
import {createOptions, createURL} from '../utils/fetch';

const getEvent = async (id) => {
    const response = await fetch(createURL('/api/events/getEvent/', id), createOptions('GET'));
    return await response.json();
}

const getUserDocIdAsync = async (body) => {
    try {
        const response = await fetch(createURL('/api/users/getAttendant', ''), createOptions('POST', body));
        return await response.json();
    } catch (e) {
        console.log(e);
    }

}

const updatePreregistration = async (body) => {
    try {
        const response = await fetch(createURL('/api/preRegistration/create', ''), createOptions('POST', body));
        return await response.json();
    } catch (e) {

    }
}

export {getEvent, getUserDocIdAsync, updatePreregistration};