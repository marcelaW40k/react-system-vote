const BASE_URL = "http://54.198.169.142:8080";

/**
 *
 * @param {String} extension - endpoint extension
 * @param {String} id - query params
 */
const createURL = (extension, id) => {
    if(id === ''){
        return new URL(BASE_URL.concat(extension));
    } else {
        return new URL(BASE_URL.concat(extension, id));
    }
};


/**
 *
 * @param {String} extension - endpoint extension
 * @param {Object} [params] - query params
 */
const createObjectURL = (extension, params) => {
    const url = new URL(BASE_URL + extension);
    if (params) {
        Object.keys(params).forEach(key => !!params[key] && url.searchParams.append(key, params[key]));
    }
    return url;
};

/**
 *
 * @param {String} method - HTTP Method, it could be POST | GET | PUT | PATCH | DELETE
 * @param {Object} [body] - request body
 */

const createOptions = (method, body) => {
    const requestOptions = { method };
    if (body) {
        requestOptions.headers = {'content-type': 'application/json'};
        requestOptions.body = JSON.stringify(body);
    }

    return requestOptions;
};

export {createURL, createOptions, createObjectURL};
