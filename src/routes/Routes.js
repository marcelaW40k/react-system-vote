import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import FormVote from '../components/FormVote';
import FormRegister from '../components/FormRegister';

function AppRouter() {
  return(
    <Router>
      <Routes>
        <Route exact path="/" element={<FormRegister/>} />
        <Route exact path="/formvote" element={<FormVote/>}/>
      </Routes>
    </Router>

  )
  
}

export default AppRouter;